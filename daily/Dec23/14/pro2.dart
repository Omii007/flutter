// Future, Async, Await :

Future<String> getOrder(){
  return Future(() => 'Burger');
}

Future<String> getOrderMessage(){

  return Future.delayed(Duration(seconds: 5),()=> getOrder());
}

void main()async{

  print("Start");
  print(await getOrderMessage());
  print("End");
}