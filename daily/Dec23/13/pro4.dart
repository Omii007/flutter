void fun1(){

  for(int i=0;i<10;i++){
    print("In fun1");
  }
}

void fun2(){
  for(int i=0;i<10;i++){
    print("In fun2 One");
  }
  Future.delayed(Duration(seconds:5),);
  for(int i=0;i<10;i++){
    print("In fun2 Two");
  }
}

void main(){

  print("Start");
  fun1();
  fun2();
  print("End");
}