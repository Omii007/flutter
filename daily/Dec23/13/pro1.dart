
/* Asynchronus Programming :
Keyword :
  - Future(class)
  - async(keyword)
  - await(keywprd)
*/
void fun(){
  print("In fun");
}

void main(){
  print("Statement 1");
  print("Statement 2");
  print("Statement 3");
  fun();
  print("Statement 4");
  print("Statement 5");
}