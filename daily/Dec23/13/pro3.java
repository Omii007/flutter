
// Async programming in other language:

class ThreadDemo extends Thread {

    public void run(){
        System.out.println(Thread.currentThread());
    }
}
class Client {
    public static void main(String[] args){

        ThreadDemo obj = new ThreadDemo();
        obj.start();
        System.out.println(Thread.currentThread());
    }
}