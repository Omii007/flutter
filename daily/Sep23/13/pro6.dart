

// RealTime

import 'dart:io';
import 'dart:core';

class Movie {

	String? name;
	double? rating;
	double? coll;

	void MovieInfo(){

		print(name);
		print(rating);
		print(coll);
	}
}
void main(){

	Movie obj = new Movie();

	print("Enter movie name:");
	obj.name = stdin.readLineSync();
	
	print("Enter movie rating:");
	obj.rating = double.parse(stdin.readLineSync()!);
	
	print("Enter movie collection:");
	obj.coll = double.parse(stdin.readLineSync()!);

	obj.MovieInfo();
}
