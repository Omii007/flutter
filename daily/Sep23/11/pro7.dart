
// Using Recursion : Factorial Number

int mult = 1;
void fact(int x){

	if(x == 0)
		return;

	mult = mult * x;
	fact(x-1);
}
void main(){

	fact(5);
	print(mult);
}
