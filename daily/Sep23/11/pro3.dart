
// Recursion

void fun(int x){

	if(x == 0){

		return;
	}
	print(x);
	fun(x-1);
}
void main(){

	fun(5);
}
