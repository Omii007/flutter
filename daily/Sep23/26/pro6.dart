

// Types Of Constructor
//1) Default Constructor

class Employee {

	int? empId;
	String? empName;

	Employee(){

		print("Default Constructor");
	}
}

void main(){

	Employee obj = new Employee();
}
