

// Standard Way Of Constructor

class company {

	int? empCount;
	String? compName;

	company(this.empCount,this.compName);

	void compInfo(){

		print(empCount);
		print(compName);
	}
}
void main(){

	company obj1 = new company(100,"Veritas");
	company obj2 = new company(200,"Pubmatic");

	obj1.compInfo();
	obj2.compInfo();
}
