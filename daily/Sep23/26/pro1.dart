
class Demo{

	int x = 50;

	Demo(){     // Demo(Demo this)

		print("In Constructor");
	}
}

void main(){

	Demo obj = new Demo();

	// Object create
	// address copy = obj 100
	// Demo(obj)    Demo(100)
}
