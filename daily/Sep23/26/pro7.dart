

// Types Of Constructor
//2) Parameterized Constructor

class Employee {

	int? empId;
	String? empName;

	Employee(this.empId,this.empName){

		print("Para Constructor");
	}
}

void main(){

	Employee obj = new Employee(18,"Virat");
}
