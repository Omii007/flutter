

// class variable or static variable

class Demo {

	int x = 10;
	static int y = 20;

	void printdata(){

		print(x);
		print(y);
	}
}
void main(){

	Demo obj1 = new Demo();

	obj1.printdata();

	Demo obj2 = Demo();

	Demo.y = 50;
	obj2.printdata();
}
