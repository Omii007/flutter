

class Employee{

	int id = 10;
	String? name = "Kanha";
	double sal = 1.2;

	void empInfo(){

		print(id);
		print(name);
		print(sal);
	}
}
void main(){

	Employee obj = new Employee();

	obj.empInfo();

	Employee obj2 = new Employee();
	obj2.empInfo();
}
