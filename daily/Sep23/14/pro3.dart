
// Class Variables and Instance Variable


class company{

	int empcount = 500;
	String compname = "Google";
	String loc = "Pune";

	void compInfo(){

		print(empcount);
		print(compname);
		print(loc);
	}
}
void main(){

	//Object1
	company obj = new company();
	obj.compInfo();

	//Object2
	company obj2 = company();
	obj2.compInfo();

	//Object3
	new company().compInfo();

	//Object
	company().compInfo();
}
