
// Operator in dart : 
		
/*	Operator :
	1) Unary   => ++a
	2) Binary  => a+b
	3) Ternary  =>  (a<b)?a:b
*/

// Unary Operator:

// Pre & Post Increment

void main(){

	int x = 5;

	print(++x); //6
	print(x++); //6
	print(x);   //7
}
