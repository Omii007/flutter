
// Relational Operator 
// {<, >, <=, >=, ==, !=}
// return value is boolean

void main(){

	int x = 10;
	int y = 8;

	print(x < y);
	print(x > y);
	print(x <= y);
	print(x >= y);
	print(x == y);
	print(x != y);
}
