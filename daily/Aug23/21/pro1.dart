/* BitWise Operator 
(& , | , ^ , << , >>)
& -> bitwise AND
| -> bitwise OR
^ -> bitwise XOR
<< -> bitwise left-shift
>> -> bitwise right-shift
*/

// Bitwise AND(&)

void main(){

	int x = 14;
	int y = 72;

	print(x &y);
}
