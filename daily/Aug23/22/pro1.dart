
// Control Statement in Dart
//(if, if-else, for, while, do-while, switch, break, continue)

// if statement

void main(){

	int x = 7;
	print(x);

	if(x < 10)
		print("x is less than 10");
	
	print("End main");
}
