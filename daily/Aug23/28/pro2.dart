
/* Functions in dart
1) User defined
2) Pre defined

Types of function :
1) No Parameter -> No return value
2) Parameter -> No return value
3) No Parameter -> retrurn value
4) Parameter -> return value
*/

void fun(){

	print("In fun");
}
void main(){

	print("Start main");
	fun();
	print("End main");
}
