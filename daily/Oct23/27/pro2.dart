
/* add
1) add
2) addAll
3) insert
4)insertAll
5) replaceRange
*/

import 'dart:ffi';

void main(){

  var Prolang = List.empty(growable: true);

  Prolang.add("Cpp");
  Prolang.add("Java");
  Prolang.add("Python");
  Prolang.add("Dart");

  print(Prolang);

  var Lang = ["ReactJs","SpringBoot","Flutter"];

  // addAll
  Prolang.addAll(Lang);
  print(Prolang);

  //insert
  Prolang.insert(3, "Go");
  print(Prolang);

  //insertAll
  Prolang.insertAll(2,["Swift","Kotlin"]);
  print(Prolang);

  //replaceRange
  Prolang.replaceRange(3,5,["Go","Dart"]);
  print(Prolang);
}