
/* Access
1) elementAt
2) getRange
3) indexOf -> first index return krto
4) indexWhere  -> index return krto
5) lastindexOf
*/

void main(){

  var Prolang = List.empty(growable: true);

  Prolang.add("CPP");
  Prolang.add("Java");
  Prolang.add("Python");
  Prolang.add("Dart");
  Prolang.add("Java");

  print(Prolang);
  print(Prolang[2]);
  print(Prolang.elementAt(3));
  print(Prolang.getRange(0, 4));
  print(Prolang.indexOf("Java"));
  print(Prolang.lastIndexOf("Java"));
  print(Prolang.indexWhere((element) => element.startsWith("P")));
}