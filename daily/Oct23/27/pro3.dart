
/* remove
1) remove
2) removeAll
3) removeLast
4) removeRange
5) removeWhere
6) clear
*/

void main(){

  var lang = ["CPP","JAVA","PYTHON","DART","C"];

  //remove
  lang.remove("C");
  print(lang);

  //removeLast
  lang.removeLast();
  print(lang);

  //removeAt
  lang.removeAt(1);
  print(lang);

  //removeRange
  lang.removeRange(0, 1);
  print(lang);

  //clear
  lang.clear();
  print(lang);
}