               /* Collection
            
  List                        Set                    LinkedList      Queue                  Map

  1) ListaBase                1)HashSet                              1)ListQueue            1)HashMap
    1) UnmodifiableListView   2) LinkedHashSet                       2)DoubledLinkedQueue   2)LinkedHashMap
                              3)SplayTreeSet                                                3)SplayTreeMap
                              4)UnmodifiableSetView                                         4)UnmodifiableMapView
                                (Parent -: SetBase)                                           (Parent -: MapView)
                                                                                            5)UnmodifiableMapBase
                                                                                              (Parent -: MapBase)
*/