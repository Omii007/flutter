
// Singleton Design Pattern

class Demo {

  static Demo obj = new Demo._private();

  Demo._private(){

    print("In constructor");
  }
  factory Demo (){

    return Demo._private();
  }
}