
// var runtime la data identify karto.
// List, Set, Map use kela tr runtime la data dynamic dhakvto.

import 'dart:collection';

void main(){

  var PlayerInfo = HashMap();  // insertion order ne O/P det nahi

  // 1st way 
  PlayerInfo[18] = "Virat";
  print(PlayerInfo);

  // 2nd way
  PlayerInfo.addAll({7:"MSD"});
  print(PlayerInfo);

  // 3rd way
  PlayerInfo.addEntries({1:"Kl Rahul",77:"Shubman",45:"Rohit"}.entries);
  print(PlayerInfo);

  // .entries he property ahe aani tyachi return value Iterable ahe.
}