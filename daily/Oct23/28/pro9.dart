
// LinkedList :

// LinkedList madhe compulsary data 'LinkedListEntry' type cha asla paheje.

import 'dart:collection';

final class company extends LinkedListEntry<company>{

  int empCount;
  String compName;
  double rev;

  company(this.empCount,this.compName,this.rev);

  String toString(){

    return "$empCount $compName $rev";
  }
}

void main(){

  var compInfo = LinkedList<company>();

  compInfo.add(new company(1000, "Veritas",1000.00));
  compInfo.add(new company(500, "Cummins",1500.00));
  compInfo.add(new company(1000,"VMWare", 2000.00));

  print(compInfo);

  print(compInfo.first);
  print(compInfo.length);
}