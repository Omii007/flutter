
// UnmodifiableMapView

import 'dart:collection';

void main(){

  var Player = LinkedHashMap();

  Player.addEntries({1:"KL Rahul",18:"Virat",45:"Rohit"}.entries);
  var PlayerInfo = UnmodifiableMapView(Player);

  print(PlayerInfo);

  PlayerInfo[7] = "MSD"; // cannot modify unmodifiable map
  print(PlayerInfo);
}