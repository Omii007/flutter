
// LinkedHashMap
// O/P insertion order ne deto

import 'dart:collection';

void main(){

  var PlayerInfo = LinkedHashMap();  // insertion order ne O/P deto

  // 1st way 
  PlayerInfo[18] = "Virat";
  print(PlayerInfo);

  // 2nd way
  PlayerInfo.addAll({7:"MSD"});
  print(PlayerInfo);

  // 3rd way
  PlayerInfo.addEntries({1:"Kl Rahul",77:"Shubman",45:"Rohit"}.entries);
  print(PlayerInfo);

  // .entries he property ahe aani tyachi return value Iterable ahe.
}