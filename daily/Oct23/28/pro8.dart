
// Queue : (Sequence) Double ended queue

import 'dart:collection';

void main(){

  var data = Queue();
  print(data.runtimeType);

  //addAll
  data.addAll([10,20,30,40,50]);

  print(data);

  //addFirst
  data.addFirst(60);

  //addLast
  data.addLast(80);

  print(data);
  print(data.length);
  print(data.last);
  print(data.first);
  print(data.isEmpty);
}