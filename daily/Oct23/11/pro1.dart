

abstract class Demo {

  Demo(){

    print("Constructor Demo");
  }
  void fun1(){

    print("In fun1");
  }
  void fun2();
}

class DemoChild extends Demo {

  DemoChild(){

    print("In DemoChild Constructor");
  }
  void fun2(){

    print("In fun2");
  }
}

void main(){

  Demo obj = new DemoChild();
  obj.fun1();
  obj.fun2();
}