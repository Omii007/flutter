

// Interface
// Interface banlyavr Inheritance hota nahi phkt method as a abstract mnun yetat.
// implement kelyvr class chya all method abstract hotat.

abstract class Developer {

  void develop (){

    print("We build software");
  }
  void devtype();
}

class MobileDev implements Developer {
 // implement kelyvr compulsary jya class la implement kely tyachya all method la override karav lagt.
  void devtype(){

    print("Flutter dev");
  }
  void develop(){

    print("Flutter develop");
  }
}

void main(){

  Developer obj = new MobileDev();
  obj.devtype();
  obj.develop();
}