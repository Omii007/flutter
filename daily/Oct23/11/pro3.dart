
abstract class Developer {

  void developer(){

    print("We build software");
  }
  void devType();
}

class MobileDev extends Developer {

  void devType(){

    print("FornEnd developer");
  }
}

class WebDev extends Developer {

  void devType(){

    print("Flutter Developer");
  }
}

void main(){

  Developer obj = new MobileDev();
  obj.developer();
  obj.devType();

  Developer obj1 = new WebDev();
  obj1.developer();
  obj1.devType();

  WebDev obj2 = new WebDev();
  obj2.developer();
  obj2.devType();

  // Developer obj3 = new Developer(); // abstarct class cha object banat nahi
  // obj3.developer();
  // obj3.devType();
}