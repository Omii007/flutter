
// Passing Argument To Child Constructor

class Parent {

  int? x;
  String? name;

  Parent(this.x,this.name);  

  void printData(){

    print(x);
    print(name);
  }
}
class Child extends Parent {

  int? y;
  String? str;

  Child(this.y,this.str,int x,String name):super(x,name);

  void dispData(){

    print(y);
    print(str);
  }
}

void main(){

  Child obj = new Child(10, "Kanha",12,"Rahul");
  obj.printData();
  obj.dispData();
}