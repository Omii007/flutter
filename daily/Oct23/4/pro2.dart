
class Parent {    // Base, Super, Parent Class

  Parent(){

    print("Parent Constructor");
  }
}
class Child extends Parent {   // Derived, SubClass, Child Class

  Child(){

    super();
    print("Child Constructor"); 
  }
}

void main(){

  Child obj = new Child();
}