
// Hierarchical Inheritance :(Parent che multiple child asu shaktat)

class Parent {

  Parent(){

    print("In parent Constructor");
  }
}
class Child1 extends Parent {

  Child1(){

    print("In Child1 Constructor");
  }
}
class Child2 extends Parent {

  Child2(){

    print("In Child2 Constructor");
  }
}

void main(){

  Parent obj = new Parent();
  Child1 obj1 = new Child1();
  Child2 obj2 = new Child2();
  
  }