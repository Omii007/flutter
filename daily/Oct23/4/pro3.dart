
class Parent {    // Base, Super, Parent Class

  Parent(){

    print("Parent Constructor");
  }
  call(){

    print("In Call Method");

  }
}
class Child extends Parent {   // Derived, SubClass, Child Class

  Child(){

    super();  // super chi line lehaychi asel tr compulsary call method lehavi lagte karan flutter madhe "super()" parent chya constructor la call krt nahi
    print("Child Constructor"); 
  }
}

void main(){

  Child obj = new Child();
  obj();  // call method la call jato.
}