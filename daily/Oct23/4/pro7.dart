
// Passing Argument To Child Constructor

class Parent {

  int? x;
  String? name;

  Parent(this.x,this.name);  

  void printData(){

    print(x);
    print(name);
  }
}
class Child extends Parent {

  int? y;
  String? str;

  Child(this.y,this.str);

  void dispData(){

    print(y);
    print(str);
  }
}

void main(){

  Child obj = new Child(10, "Kanha");
}