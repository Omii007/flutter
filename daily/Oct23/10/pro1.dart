
// Polymorphism
// Overriding

class Parent {

  void career(){

    print("Engg");
  }
  void marry(){

    print("Depika Padukonee");
  }
}
class Child extends Parent {

  void marry(){

    print("Disha Patni");
  }
}

void main(){

  Parent obj = new Child();
  obj.career();
  obj.marry();
}