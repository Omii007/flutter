
// Abstract Classes :

abstract class Parent {

  void Property(){

    print("Gold,Bunglow,flats,cars");
  }
  void career();
  void marry();
}

class Child extends Parent {

  void career(){

    print("Engg");
  }
  void marry(){

    print("Salena");
  }
}

void main(){

  Parent obj = new Child();
  obj.career();
  obj.marry();
  obj.Property();
}