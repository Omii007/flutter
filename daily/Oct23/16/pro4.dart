

// mixin

mixin DemoParent {

  void m1(){

    print("In m1- DemoParent");
  }
}

class Demo{


  void m1(){

    print("In m1- Demo");
  }
}

class Demochild extends Demo with DemoParent{


}
void main(){

  Demochild obj = new Demochild();
  obj.m1();

} 