
/* Properties
1) first
2) hashCode
3) isEmpty
4) isNotEmpty
5) iterator
6) last
7) length
8) reversed
9) runtimeType
10)
single
*/

void main(){

  List Player = ["Virat","Rohit","KL Rahul","Shami"];

  print(Player);
  print(Player.first);
  print(Player.hashCode);
  print(Player.isEmpty);
  print(Player.isNotEmpty);
  print(Player.iterator); 
  print(Player.last);
  print(Player.length);
  print(Player.reversed);
  print(Player.runtimeType);
  print(Player.single);

}