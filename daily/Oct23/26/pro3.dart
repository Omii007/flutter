
// var ha keyword use kela aani list madhe multiple type cha data dela tr asel tr to List<Object> deto.
// var generic deta deu shkt nahi karan to runtime la check krto
// List use kel tr to List<dynamic> deto runtimeType
void main(){

  var empData = [10,"Kanha","BMC Software",1.5,10,"Ashish","BMCSoftware",2.0];
  List empData1 = [10,"Kanha","BMC Software",1.5,10,"Ashish","BMCSoftware",2.0];
  print(empData);
  print(empData.runtimeType);
  print(empData1.runtimeType);
}