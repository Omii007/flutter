
// Unmodifiable Constructor :

void main(){

  List Player = List.unmodifiable([10,20,30]);

  print(Player);
  Player[0] = 50; // cannot modify an unmodifiable list 
  // read only access
}