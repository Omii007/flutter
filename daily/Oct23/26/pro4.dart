
/* Constructor in dart :
1) empty
2) filled
3) from
4) generate
5) of
6) unmodifiable
*/

// Empty Constructor :(Fixed size)

void main(){

  List Player1 = List.empty(); // cannot add to a fixed-length list
 // Player1.add("Virat");

 List Player2 = List.empty(growable: true);

 print(Player2);
 Player2.add("Virat");
  Player2.add("Rohit");
  print(Player2);
// List madhe eka index var value thevychi mnl tr to override krto.
  Player2[0] = "Shubman";
  print(Player2);


}