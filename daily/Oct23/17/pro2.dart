
// mixin cha object banat nahi & abstract class cha pn object bant nahi
// mixin bydefault abstract asto.

mixin Demo1 {

  Demo1(){  // mixin cha explicitly constructor banvu shkt nahi

    print("In Constructor");
  }
  void fun1(){

    print("In fun1-Demo1");
  }
  void fun2();
}

void main(){

  Demo1 obj = new Demo1();  // mixin bydefault abstract asto mnun tyacha object banat nahi.
} 