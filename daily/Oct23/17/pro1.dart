
abstract class Demo1 {

  void fun1(){

    print("In Demo1-fun1");
  }
  void fun2();
}

abstract class Demo2 {

  void fun3(){

    print("In Demo2-fun3");
  }
  void fun4();
}

class ChildDemo implements Demo1,Demo2 {
   // ekhada class implements krt asu tr tya class chya all method la override karavch lagt child class madhe nahitr error yete.
  void fun2(){

    print("In childDemo-fun2");
  }
  void fun4(){
    print("In ChildDemo fun4");
  }
}

void main(){

  ChildDemo obj = new ChildDemo();
  obj.fun1();
  obj.fun2();
  obj.fun3();
  obj.fun4();
}