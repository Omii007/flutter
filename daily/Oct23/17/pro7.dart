
class Parent {

  void m1(){

    print("In m1-Parent");
  }
}
mixin Demo on Parent {
 // mixin varti 'on' vaprl ki tychya Parent and Parent chya Child la functionality melte.
 // mnjech Parent chya hierarkey madhlyana functionality melnar.
  void fun1(){

    print("In fun1-Demo");
  }
}

class Normal with Demo {
  // 'with Demo' yala error ahe karan aapn mixin direct class sobat bind kru shkt nahi tyala visversa through use karav lagt. 
} 
void main(){

  Normal obj = new Normal();
  obj.fun1();
}