
mixin Demo1 {

  void fun1(){

    print("In fun1-Demo1");
  }
}

// 'on' ha keyword extends sarkha ahe pn to method cha sachya gheun yeto.
mixin Demo2 on Demo1 {
 // on mnl ki abstract ghosti yetat.
  void fun2(){

    print("In fun2-Deo2");
  }
}

class Normal with Demo2 {


}
void main(){

  Normal obj = new Normal();
  obj.fun1();
  obj.fun2();
}