
/* abstarct class nusar multiple inheritance support karat nahi pn mixin through multiple inheritance support kart with the help of keyword(with).

->  interface madhe child kade all method override karavya lagtat, pn mixin madhe all method override karavya lagat nahi.  
->  interface madhe jya method la body nahi tyanach override karav lagt.
*/

mixin Demo1 {

  void fun1(){

    print("In fun1-Demo1");
  }
}
mixin Demo2 {

  void fun2(){

    print("In fun2-Demo2");
  }
}

class DemoChild with Demo1,Demo2 {


}

void main(){

  DemoChild obj = new DemoChild();

  obj.fun1();
  obj.fun2();
}