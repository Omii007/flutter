
// 2nd way of Getter Method

class Demo {

  int? _x;
  String? str;
  double? _sal;

  Demo(this._x,this.str,this._sal);

 get getX {
    return _x;
  }
  get getSal {
    return _sal;
  }
  void Info(){
    print(_x);
    print(str);
    print(_sal);
  }
}