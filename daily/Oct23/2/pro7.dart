
// Setter method data set krte mhanun tela return type nasto mnje void asto.
// Getter method data kadhun dete mnun tela return type asto , jya type cha deta ahe to type asto

// 1st way of Setter

class Demo{

  int? _x;
  String? str;
  double? _sal;

  Demo(this._x,this.str,this._sal);

  void setX(int x){
    _x = x;
  }
  void setStr(String str){
    this.str = str;
  }
  void setSal(double sal){
    _sal = sal;
  }
  void Info(){
    print(_x);
    print(str);
    print(_sal);
  }
}