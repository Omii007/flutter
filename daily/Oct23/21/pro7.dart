
abstract class Developer {

  factory Developer(String devType){

    if(devType == "Backend")
      return Backend();
    
    else if(devType == "Frontend")
      return Frontend();

    else if(devType == "Mobile")
      return Mobile();

    else;
      return other();
  }
  void devlang();
}

class Backend implements Developer {

  void devlang(){

    print("NodeJs/SpringBoot");
  }
}
class Frontend implements Developer {

  void devlang(){

    print("ReactJs/AngularJs");
  }
}

class Mobile implements Developer {

  void devlang(){

    print("Flutter/Android");
  }
}

class other implements Developer {

  void devlang(){

    print("Testing/Devops/support");
  }
}

void main(){

  Developer obj1 = new Developer("Frontend");
  obj1.devlang();

  Developer obj2 = new Developer("Backend");
  obj2.devlang();

  Developer obj3 = new Developer("Mobile");
  obj3.devlang();

  Developer obj4 = new Developer("Devops");
  obj4.devlang();



}