
class Demo {

  static Demo obj = new Demo();

// constructor has no return type
  Demo Demo(){

    print("In constructor");
    return obj;
  }
}

void main(){

  Demo obj = new Demo();
}