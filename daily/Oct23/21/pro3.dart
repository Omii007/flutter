
// Factory Constructor
// factory constructor abstraction aanyasathi vaprla jato
// compulsary class cha or child cha object return karava lagto.
// factory constructor & constructor ch name different asav.
// In factory constructor this are not allowed.
// Factory constructor la 1st parameter this chalat nahi.

class Demo {

  Demo._private(){

    print("In constructor");
  }
  factory Demo (){
    print("In constructor Demo");
    return Demo._private();

    // Internally
    // Demo obj = new Demo._private();
    // return Demo._private();
    //Or  return object;
  }
}
