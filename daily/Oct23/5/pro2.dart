
class Company {

  String? compName;
  String? Loc;

  Company(this.compName,this.Loc);

  void compInfo(){

    print(compName);
    print(Loc);
  }
}

class Employee extends Company {

  int? empId;
  String? empName;

  Employee(this.empId,this.empName,String compName,String Loc):super(compName,Loc);

  void empInfo(){

    print(empId);
    print(empName);
  }

}

void main(){

  Employee obj = new Employee(15, "Kanha", "TCS", "Mumbai");

  obj.compInfo();
  obj.empInfo();
}