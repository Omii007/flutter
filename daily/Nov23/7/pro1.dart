
// # 45
// finally :(connection close krnya sathi use hoto)
import 'dart:io';

void main(){

  int? x;

  try{

    print("Connection open");
    print("Enter a value");
    x = int.parse(stdin.readLineSync()!);

    print(x);
  }on FormatException{

    print("Wrong Input");
  }catch(ex){

    print(ex);
  }finally{

    print("Connection closed");
  }
  print("End main");
}