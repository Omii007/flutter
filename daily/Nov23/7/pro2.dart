
// throw:(user defined and pre defined exception pheknyasathi use hoto)

//1) throwing pre defined exception

import 'dart:io';

void main(){

  int empCount = int.parse(stdin.readLineSync()!);
  String? name = stdin.readLineSync();
  int profit = int.parse(stdin.readLineSync()!);

  try{

    if(profit < 0){
      throw new FormatException();
    }
  }on FormatException{

    print("Laksh de company loss madhe ahe");
  }catch(data){

    print("Generic Exception");
  }

  print("$empCount $name $profit");
}