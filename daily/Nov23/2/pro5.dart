

// Iterable Methods

void main(){

  var player = ["Rohit","Shubman","Virat","KL Rahul","Shreyas","Hardik"];

  //every(bool test(E element))-> bool
  // Check whether every element of this iterable satisfies test.

  var retval = player.every((element) => element == "R");

  print(retval);

  var ret = player.every((element) => element.length > 4);

  print(ret);
}