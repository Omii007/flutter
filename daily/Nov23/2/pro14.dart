

// Iterable Methods

void main(){

  var player = ["Rohit","Shubman","Virat","KL Rahul","Shreyas","Hardik"];

  //singleWhere(bool test(E elemet,{E or Else()?}))-> E
  // The single element that satisfies test

  var retval = player.singleWhere((element) => element.length == 6);

  print(retval);

}