
// Iterable Methods

void main(){

  var player = ["Rohit","Shubman","Virat","KL Rahul","Shreyas","Hardik"];

  //take(int count)-> iterable<E>
  // Creates a lazy iterable of the count first elements of this iterable

  var retval = player.take(3);

  print(retval);
  // count deyel tevdhe element print hotyt.
}