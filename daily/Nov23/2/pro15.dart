

// Iterable Methods

void main(){

  var player = ["Rohit","Shubman","Virat","KL Rahul","Shreyas","Hardik"];

  //skip(int count)->Iterable<E>
  // Creates an iterable that provides all but the first count elements.

  var retval = player.skip(4);

  print(retval);
  // count javdha deu tevde element skip hotat.
}