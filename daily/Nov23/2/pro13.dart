
// Iterable Methods

void main(){

  var player = ["Rohit","Shubman","Virat","KL Rahul","Shreyas","Hardik"];

  // reduce(E combine (E value,e element)-> E) (fold and reduce same ahet)
  // Reduce a collection to single value by iteratively combining elements of the collection using the provided function.

  var retval = player.reduce((value, element) => value+element);

  print(retval);
  // same as fold function string concat krtyt. 
}