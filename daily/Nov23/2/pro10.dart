
// Iterable Methods

void main(){

  var player = ["Rohit","Shubman","Virat","KL Rahul","Shreyas","Hardik"];

  // forEach(void action(E element))-> void
  // invokes action on each element of this iterable in iteration order.

  var retval = player.forEach(print);

  // return type void ahe forEach cha
}