
// Iterable Methods

void main(){

  var player = ["Rohit","Shubman","Virat","KL Rahul","Shreyas","Hardik"];

  //toList({bool growable = true})-> List<E>
  // Create a list containing the elements of this iterable.

  var retval = player.toList();

  print(retval);
  // it can convert set into list
}