
// Iterable Methods

void main(){

  var player = ["Rohit","Shubman","Virat","KL Rahul","Shreyas","Hardik"];

  //followedBy(Iterable<E> other)-> Iterable<E>.
  // Creates the lazy concatenation of this iterable & other.

  var retval = player.followedBy(["Jadeja","Shami"]);

  print(player);
  print(retval);
  // last la element add krto.
}