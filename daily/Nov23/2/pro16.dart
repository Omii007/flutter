
// Iterable Methods

void main(){

  var player = ["Rohit","Ravindra","Shubman","Virat","KL Rahul","Shreyas","Hardik"];

  // skipWhile(bool test(E value))-> iterable <E>
  // Creates an Iterable that skips leading elements while test is satisfied.

  var retval = player.skipWhile((value) => value.length == 5);

  print(retval);

  var ret = player.skipWhile((value) => value[0] == "R");

  print(ret);
  // Random data skip krto startingla ch asel tr
}