
// Iterable Methods

void main(){

  var player = ["Rohit","Shubman","Virat","KL Rahul","Shreyas","Hardik"];

  //where(bool test(E element))-> Iterable<E>
  // Creates a new lazy Iterable with all elements that satisfy the predicate test

  var retval = player.where((element) => element.length > 5);

  print(retval);

  var ret = player.where((element) => element[0] == "R");

  print(ret);
  // return element or list return krto.
}