

// Iterable Methods

void main(){

  var player = ["Rohit","Shubman","Virat","KL Rahul","Shreyas","Hardik"];

  // lastWhere(bool test(E element),{E or Else()?})-> E
  //The last element that satisfies the given predicate test.

  var retval = player.lastWhere((element) => element.length == 5);

  print(retval);

  var ret = player.lastWhere((element) => element[0] == "S");

  print(ret);

  // last element varti work krto.
}