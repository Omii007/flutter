

// Iterable Methods

void main(){

  var player = ["Rohit","Shubman","Virat","KL Rahul","Shreyas","Hardik"];

  // firstWhere(bool test(E element),{E or Else()?})-> E
  // The first element that satisfies the given predicate test.

  var retval = player.firstWhere((element) => element[0] == "H");

  print(retval);

  var ret = player.firstWhere((element) => element.length > 5);
  print(ret);
}