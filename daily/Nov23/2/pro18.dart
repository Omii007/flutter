

//Iterable Methods

void main(){

  var player = ["Rohit","Shubman","Virat","KL Rahul","Shreyas","Hardik"];

  //takeWhile(bool test(E value))-> Iterable <E>
  //Create a lazy iterable of the leading elements satisfying test

  var retval = player.takeWhile((value) => value[0] == "R");

  print(retval);
  // List madhil first element match zala trch O/P dete nahitr list madhil konta he element cha varaible del tr O/P det nahi
}