
// Iterable Methods

void main(){

  var player = ["Rohit","Shubman","Virat","KL Rahul","Shreyas","Hardik"];

  // fold<T>(T initialValue,T combine(T previousValue,E element))-> T
  // Reduces a collection to a single value by iteratively combining each element of the collection with an existing value.

  var initial = "";

  var retval = player.fold(initial, (previousValue, element) => previousValue+element);

  print(retval);
  // value concat krto.
}