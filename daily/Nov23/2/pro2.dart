
// Iterable Methods

void main(){

   var player = ["Rohit","Shubman","Virat","KL Rahul","Shreyas","Hardik"];

   // any(bool test(E element))-> bool
   // checks whether any element of this iterable satisfies test.

   var retval = player.any((element) => element.length == 5);

   print(retval);

   var ret = player.any((element) => element[0] == "V");

   print(ret);
}