
// Iterable Methods

void main(){

  var player = ["Rohit","Shubman","Virat","KL Rahul","Shreyas","Hardik"];

  //map<T>(T toElement(E e)-> iterable <T>) ***** IMP
  // The current elements of this iterable modified by toElement

  var retval = player.map((e) => e+"Ind");

  print(retval);
  // element la other string concat krto.
}