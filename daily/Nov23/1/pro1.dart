
// Using iterator on user defined class :

class ProgramLang implements Iterator {

  int index = -1;
  var proLang = [];

  ProgramLang(String lang){

    this.proLang = lang.split(",");
  }

  bool moveNext(){

    if(index < proLang.length-1){

      index = index + 1;
      return true;
    }
    return false;
  }

  get current {

    if(index >= 0 && index <= proLang.length-1){

      return "${proLang[index]}";
    }
  }
  
  
 
}

void main(){

  ProgramLang obj = new ProgramLang("CPP,JAVA,PYTHON,DART");

  while(obj.moveNext()){

    print(obj.current);
  }
}