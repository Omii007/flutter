
// Exception Handling

import 'dart:io';
void main(){

  print("Start main");
  try{

    int x = int.parse(stdin.readLineSync()!);
    print(x);

  }catch(ex){

    print(ex);
  }
  print("End main");
}