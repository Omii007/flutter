

import 'dart:io';

void main(){

  print("Start main");

  try{

    print("Enter a value");

    int x = int.parse(stdin.readLineSync()!);

    print(x);
  }on FormatException{

    print("Handled Exception");
  }catch(ex){

    print(ex);
  }
  print("End main");
}