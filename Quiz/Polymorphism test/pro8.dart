
// Will the code work ?

class test {

  int x = 10;
  static int y = 20;

  test();
  void fun(){

    print(x);
    print(y);
  }
}

class test2 extends test {

  int x = 10;
  int y = 21;

  test2();

  void fun(){

    print(x);
    print(y);
  }
}

void main(){

  test2 obj = test();
  obj.fun();
}