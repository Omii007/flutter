
// What must be add at the place of "add missing line here" to access the method of accelerate() of class car ?

class Vehicle {

  void accelerate(){

    print("vehicle is accelerating");
  }
}

class Car extends Vehicle {

  void accelerate(){

    print("car is accelerating");
  }
}

void startAcceleration (Vehicle vehicle){

  vehicle.accelerate();
}

void main(){

  Vehicle vehicle = Car();
  startAcceleration(vehicle); // missing line is startAcceleration(vehicle)
}