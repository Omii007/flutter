

// Guess the output ?

class Company {

  void companyName(){

    print("Google");
  }
}

class Employee extends Company {

  void companyName(){

    print("Apple");
  }
}

void main(){

  Company obj = new Employee();
  obj.companyName();       // object create by Employee class to call the Employee class method ans is Apple
}