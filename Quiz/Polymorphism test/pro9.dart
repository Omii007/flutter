

// Will the given code give error?

class parent {

  static int x = 10;
  String str;

  parent(this.str);

  void printData(){

    print(x);
    print(str);
  }
} 
class child extends parent {

  int y;
  child(this.y,String str):super(str);

  static void getX(){

    print(x);
  }
}
void main(){

  child obj = new child(10, "core2web");
  obj.printData();
}