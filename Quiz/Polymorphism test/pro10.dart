
// Will this code work ?

class parent {

  int x = 10;
  static int y = 7;

  parent();

  void getData(){

    print(x);
    print(y);
  }
}

class child extends parent {

  double y = 10;
  static String str = "Core2Web";
  
  child();

  int getData(){

    print(y);
    print(str);
    return y ~/ 2;
  }
}

void main(){

  child obj = new child();
  obj.getData();
}