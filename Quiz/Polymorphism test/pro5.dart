
// what must be add at place of "add missing Line here" to access the Education() method?

abstract class parent {

  void Education();
}

class child extends parent {  // missing line is extends parent

  void Education(){

    print("To Became the doctor");
  }
}

void main(){

  parent obj = new child();
  obj.Education();
}