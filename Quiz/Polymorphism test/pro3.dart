
// what must be added in place of ?? to get output 8

class parent {

  int x = 10;

  void myData(int x){

    x = x;
  }
}

class child extends parent {

  int x = 8;

  void myData(int x){

    super.myData(x);

    super.x = this.x;  // this line has added
    this.x = x;
    print(super.x);
  }
}

void main(){

  child obj = new child();
  obj.myData(3);
}